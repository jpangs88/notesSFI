# Anteckningar for SVA2 module 1 prov

- Svenska är en nordisk språk
- Svenska, danska och nordska är vädligt like och heter scandinavian språk.
- Islandska och färöiska är annorlunda
- Utvecklades av germanska
- Först var det ett genemsant nordist språk sedan uttvecklade värianter
- Runt 800 började runsvenskan
  1. Runsvenskan 800 - 1225
  2. Fornsvenskan 1225 - 1526
  3. Nysvenskan 1526 - 1906
  4. Modern svenskan eller nutidssvenskan 1906 - nu

## Runsvenska

- Svenska var skrivit med runor
- Det fanns två varianter 16 tecken och 24 tecken
- Var gjort på minnesstenar ofta om familjen
- Varje run betyd en sak eller en fenomen som skulle har varit viktig till
  bonder
- Det tog mycket tid att skriva runor så man skrev runor bara om viktiga saker

## Fornsvenskan

- Man tror att fornsvenskan period började runt 1225 därför att Västgötalagen
  var skrivit i fornsvenskan
  - Det var en lagtext och ett officiellt document
  - Det fanns flera texter som Västgötalagen som handlade om olika delar av
    sverige
- Texter började att vara översätt från franska
- Några saker som var typisk i fornsvenskan
  - Fyra kasus
  - Tre genus
  - Verben böjdes efter person och numerus
  - Utvecklade från latinet och tyskan
    - Latin: Alfabet
    - Tyska: Suffix och prefix, bisatsform, många ord...
  - Tyskan var mäktig därför att det var använd av mycket köpman
- Fornsvenskan är mer komplicerad och är en lite som tyska
